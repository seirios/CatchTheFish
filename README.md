Catch The Fish!
==============

Course Project for the Developing Data Products course from Coursera.

A simple game using R and Shiny.

ASCII art goodness thanks to [FIGlet Server](http://www.asciiset.com/figletserver.html).

Available on ShinyApps.io as [CatchTheFish](http://seirios.shinyapps.io/CatchTheFish/).