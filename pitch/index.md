---
title       : Catch the fish!
subtitle    : A simple game using R and Shiny
author      : Sirio Bolaños Puchet
job         : Developing Data Products (part of the Data Science Specialization on Coursera)
framework   : io2012      # {io2012, html5slides, shower, dzslides, ...}
highlighter : highlight.js  # {highlight.js, prettify, highlight}
hitheme     : tomorrow      # 
widgets     : []            # {mathjax, quiz, bootstrap}
mode        : selfcontained # {standalone, draft}
knit        : slidify::knit2slides
---

## About the game

* The game is played on a 17x17 square grid and there are two pieces: the net and the fish.
* The objective is to move the net into the square where the fish is to catch it, however the net can only be moved in horizontal or vertical streaks, each of which counts as one move.
* The challenge lies in performing the exact streaks needed to reach the fish in the minimum number of moves.
* When the fish is caught one scores and a new fish appears at a random location.
* To keep a high rating one has to catch as much fish in as few moves as possible.

<center>![fish](assets/img/fish.png)</center>

---

## The game engine

* The game is implemented using a 17x17 state matrix which takes on the values: `1` (blank space/water), `2` (fish), `3` (net) and `4` (fish on net).
* The motion of the net is effectively achieved by switching back and forth the corresponding cells between states `1` and `3`.
* The initial location of the fish is computed randomly using a uniform distribution on the grid but avoiding the initial location of the net, which is always the center cell.
* When the net and the fish coincide the cell goes to state `4`, the `score` is increased by one and a new fish position is computed like the initial one but avoiding the current position of the net. The corresponding cells in the matrix are switched accordingly between states `1` and `2`.
* Apart from the `score`, we keep track of the number of `moves` and combine them in a ratio to form the `rating`, which is recorded throughout the game and some `stats` are computed on it and updated on each move.

---

## The user interface

<img src="assets/fig/gameboard.png" title="plot of chunk gameboard" alt="plot of chunk gameboard" style="display: block; margin: auto;" />

* Two sliders allow for independent but non-simultaneous horizontal and vertical motions of the net. The estimation of how long to perform a streak is purely visual, based on the current game board positions.
* On each move the game board, which is just a color-coded (<span style="color:#60C2FF;">water</span>/<span style="color:#FF9E61;">fish</span>/<span style="color:#CCCCCC;">net</span>/<span style="color:#E5DC97;">fish on net</span>) grid plot of the underlying state matrix, is updated as are the `score`, number of `moves` and `rating`.
* The time series for the `rating` is plotted on each move and a table is shown with the updated `stats` (min, max, mean, median, 1st quart. and 3rd quart.) computed on it.

---

## Play online!

* The game is available on ShinyApps.io as [CatchTheFish](http://seirios.shinyapps.io/CatchTheFish/).
* Practice your visual on-grid distance appraisal skills!
* Compete with your friends for the nicest rating time series or the best statistics!
* Or just have a good time catching some square fish!
